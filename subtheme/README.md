# SeeD Theme - Sub theme
This is the skeleton for you to create your own SeeD E.M. sub theme.
You should NOT the files as they are provided here. Instead you should modified them to create your own.

## How to use
1. Copy the whole **sub theme** folder on to your site’s folder (e.g. sites/yoursite/themes/**your_sub_theme**.
2. Rename the subtheme folder to whatever name you want for your theme (e.g. **my_fancy_theme**).
3. Rename the subtheme.info.example file to the same name as your subtheme folder (e.g. **my_fancy_theme.info**)
4. Rename the template.php.example file to just **template.php**
5. Go to the **/admin/appearance** and enable your new fancy sub theme.