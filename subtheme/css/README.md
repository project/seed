# SeeD Theme - Sub theme
The css folder is where you should put all your CSS files.
We recommend using the following structure (but it is just our way of doing things)

* CSS
    * layout.css
    * html.css
    * forms.css
    * …
    * **mobile**
        * mobile.css
        * …
    * **desktop**
        * desktop.css
        * …

